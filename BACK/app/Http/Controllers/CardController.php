<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Card;

class CardController extends Controller
{
    public function index()
    {
        // Utilisez le modèle Card pour récupérer les 10 dernières cartes triées par date de création décroissante
        $cards = Card::orderBy('created_at', 'desc')->take(10)->get();
        return response()->json($cards);
    }
}
