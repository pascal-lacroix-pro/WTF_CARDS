<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'bg_color',
        'theme_id', 
    ];

    // Relation avec la table `themes`
    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    // Relation avec la table `cards`
    public function cards()
    {
        return $this->hasMany(Card::class);
    }
}
