<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'bg_color',
        'url_logo',
        'url_img',
        'font',
        'user_id' 
    ];

    // Définir la relation avec User
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Définir la relation avec Type
    public function types()
    {
        return $this->hasMany(Type::class);
    }

}
