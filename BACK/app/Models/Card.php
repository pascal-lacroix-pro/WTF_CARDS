<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'attack',
        'defense',
        'url_img',
        'user_id',
        'type_id',
    ];

    // Relation avec la table `users`
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Relation avec la table `types`
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    // Relation avec la table `votes`
    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function deck()
    {
        return $this->belongsToMany(Deck::class);
    }
}

