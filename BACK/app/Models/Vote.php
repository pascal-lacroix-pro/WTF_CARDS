<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'card_id',
        'is_liked',
    ];

    // Relation avec la table `users`
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Relation avec la table `cards`
    public function card()
    {
        return $this->belongsTo(Card::class);
    }
}
