<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id(); // INT UNSIGNED NOT NULL AUTO_INCREMENT
            $table->string('name', 50); // VARCHAR(50) NOT NULL
            $table->string('description', 255)->nullable(); // VARCHAR(255), peut être NULL
            $table->unsignedInteger('attack'); // INT(3) UNSIGNED NOT NULL
            $table->unsignedInteger('defense'); // INT(3) UNSIGNED NOT NULL
            $table->string('url_img', 255)->default('default.png'); // VARCHAR(255) NOT NULL DEFAULT 'default.png'
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrent();
            $table->unsignedBigInteger('user_id'); // INT UNSIGNED NOT NULL
            $table->unsignedBigInteger('type_id'); // INT UNSIGNED NOT NULL

            // Définir les clés étrangères si les tables correspondantes existent déjà
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cards');
    }
};
