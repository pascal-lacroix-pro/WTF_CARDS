<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('card_user', function (Blueprint $table) {
            $table->unsignedBigInteger('card_id'); // INT UNSIGNED NOT NULL
            $table->unsignedBigInteger('user_id'); // INT UNSIGNED NOT NULL
            $table->timestamps(); // Crée created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP et updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP

            // Définir 'card_id' et 'user_id' comme clés étrangères
            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            // Créer une clé primaire composite pour éviter les doublons d'associations
            $table->primary(['card_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('card_user');
    }
};

