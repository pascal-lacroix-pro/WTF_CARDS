<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id(); // Crée automatiquement un champ 'id' UNSIGNED NOT NULL AUTO_INCREMENT
            $table->string('username', 80)->nullable(false); // Vous avez spécifié que 'username' ne doit pas être NULL
            $table->string('email', 255)->nullable(); // Vous avez spécifié que 'email' peut être NULL
            $table->string('password', 255); // Vous n'avez pas spécifié que 'password' ne doit pas être NULL, donc il ne sera pas NULL par défaut
            $table->timestamp('created_at')->useCurrent(); // Définit la valeur par défaut sur CURRENT_TIMESTAMP
            $table->timestamp('updated_at')->nullable()->useCurrent(); // Peut être NULL et utilise CURRENT_TIMESTAMP comme valeur par défaut
            $table->timestamp('email_verified_at')->nullable()->useCurrent(); // Peut être NULL et utilise CURRENT_TIMESTAMP comme valeur par défaut
        });

        // Schema::create('password_reset_tokens', function (Blueprint $table) {
        //     $table->string('email')->primary();
        //     $table->string('token');
        //     $table->timestamp('created_at')->nullable();
        // });

        // Schema::create('sessions', function (Blueprint $table) {
        //     $table->string('id')->primary();
        //     $table->foreignId('user_id')->nullable()->index();
        //     $table->string('ip_address', 45)->nullable();
        //     $table->text('user_agent')->nullable();
        //     $table->longText('payload');
        //     $table->integer('last_activity')->index();
        // });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
        // Schema::dropIfExists('password_reset_tokens');
        // Schema::dropIfExists('sessions');
    }
};
