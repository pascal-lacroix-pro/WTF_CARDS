<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('types', function (Blueprint $table) {
            $table->id(); // Laravel crée automatiquement une colonne UNSIGNED NOT NULL AUTO_INCREMENT
            $table->string('name', 50); // VARCHAR(50) NOT NULL
            $table->string('bg_color', 255); // VARCHAR(255) NOT NULL
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrent();
            $table->unsignedBigInteger('theme_id'); // INT UNSIGNED NOT NULL
            
            // Si la table `themes` existe déjà et `theme_id` est censé être une clé étrangère :
            $table->foreign('theme_id')->references('id')->on('themes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('types');
    }
};
