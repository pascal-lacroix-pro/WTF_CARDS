<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->id(); // Equivalent to INT UNSIGNED NOT NULL AUTO_INCREMENT
            $table->string('name', 100)->nullable(false);
            $table->text('description')->nullable(false);
            $table->string('bg_color', 45)->nullable(false);
            $table->string('url_logo', 255)->nullable(false);
            $table->string('url_img', 255)->nullable(false);
            $table->string('font', 255)->nullable(false);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrent();
            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themes');
    }
}
