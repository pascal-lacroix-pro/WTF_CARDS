<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id'); // INT UNSIGNED NOT NULL
            $table->unsignedBigInteger('card_id'); // INT UNSIGNED NOT NULL
            $table->tinyInteger('is_liked'); // TINYINT(1) NOT NULL
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrent();

            // Définir 'user_id' et 'card_id' comme clés étrangères
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');

            // Créer une clé primaire composite pour éviter les doublons de votes
            $table->primary(['user_id', 'card_id']);

            // Vous pouvez également vouloir indexer 'is_liked' si vous prévoyez de l'utiliser fréquemment dans les requêtes
            $table->index('is_liked');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('votes');
    }
};

