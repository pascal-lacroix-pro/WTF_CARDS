<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Type;
use App\Models\Card;

class CardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Assurez-vous que des utilisateurs et des types sont déjà présents dans la base de données pour cette plage d'ID
        $userId = User::inRandomOrder()->first()->id;
        $typeId = Type::inRandomOrder()->first()->id;

        return [
            'name' => $this->faker->word,
            'description' => $this->faker->sentence,
            'attack' => $this->faker->numberBetween(1, 100), // Exemple de plage ajustée pour attack
            'defense' => $this->faker->numberBetween(1, 100), // Exemple de plage ajustée pour defense
            'url_img' => $this->faker->imageUrl(640, 480, 'animals', true),
            'user_id' => $userId,
            'type_id' => $typeId,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
