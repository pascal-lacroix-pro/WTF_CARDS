<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Theme;

class TypeFactory extends Factory
{
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'bg_color' => $this->faker->safeHexColor,
            'theme_id' => $this->faker->randomElement(Theme::pluck('id')->toArray()),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
