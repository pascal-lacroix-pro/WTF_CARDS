<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    protected $model = User::class;

    /**
     * The current password being used by the factory.
     */
    protected static ?string $password;

    public function definition()
    {
        return [
            'username' => $this->faker->unique()->userName,
            'email' => $this->faker->unique()->safeEmail,
            'password' => Hash::make('password'), // Ici, nous hachons le mot de passe.
            'created_at' => now(),
            'updated_at' => now(),
            'email_verified_at' => $this->faker->boolean(90) ? now() : null, // 90% de chances que l'email soit vérifié.
        ];
    }
}
