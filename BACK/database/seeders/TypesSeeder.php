<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class TypesSeeder extends Seeder
{

    public function run(): void
    {
        DB::table('types')->updateOrInsert([
            ['name' => 'spectral', 'bg_color' => '#9933FF', 'theme_id' => 1],
            ['name' => 'aquatique', 'bg_color' => '#33A1FF', 'theme_id' => 1],
            ['name' => 'terrestre', 'bg_color' => '#66CC66', 'theme_id' => 1],
            ['name' => 'volant', 'bg_color' => '#FFD700', 'theme_id' => 1],
            ['name' => 'cosmique', 'bg_color' => '#8B008B', 'theme_id' => 1],
            ['name' => 'légendaire', 'bg_color' => '#FF4500', 'theme_id' => 1],

            ['name' => 'exotique', 'bg_color' => '#FF1493', 'theme_id' => 2],
            ['name' => 'force brutale', 'bg_color' => '#FF8C00', 'theme_id' => 2],
            ['name' => 'romantique', 'bg_color' => '#FF69B4', 'theme_id' => 2],
            ['name' => 'aventurier', 'bg_color' => '#228B22', 'theme_id' => 2],
            ['name' => 'gothique', 'bg_color' => '#4B0082', 'theme_id' => 2],
            ['name' => 'fashion', 'bg_color' => '#FFC0CB', 'theme_id' => 2],
            ['name' => 'retropunk', 'bg_color' => '#8A2BE2', 'theme_id' => 2], 
        ]);
    }
}
