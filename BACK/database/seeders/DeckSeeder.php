<?php
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Card;
use App\Models\Deck;

class DeckSeeder extends Seeder
{
  public function run()
  {

    $users = User::all();

    foreach ($users as $user) {

      $deck = Deck::firstOrCreate(['user_id' => $user->id]);

      $numberOfCardsToAdd = rand(5, 15);

      $cards = Card::inRandomOrder()->take($numberOfCardsToAdd)->get();

      $deck->cards()->syncWithoutDetaching($cards->pluck('id')->toArray());
    }
  }
}
