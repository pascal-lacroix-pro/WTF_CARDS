<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Type;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Assurez-vous que des thèmes sont présents dans la base de données avant d'exécuter ce seeder.
        Type::factory()->count(10)->create(); // Cela créera 10 types.
    }
}
