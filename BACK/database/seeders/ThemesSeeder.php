<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class ThemesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Assurez-vous qu'il y a déjà des utilisateurs dans la base de données.
        $userId = User::inRandomOrder()->first()->id;

        DB::table('themes')->insert([
            [
                'name' => 'RetroMonster',
                'description' => 'Un thème rétro mettant en avant les monstres.',
                'bg_color' => '#FF5733',
                'url_logo' => 'https://example.com/retromonster_logo.png',
                'url_img' => 'https://example.com/retromonster_background.jpg',
                'font' => 'Helvetica, sans-serif',
                'user_id' => $userId // Utilise un ID utilisateur existant
            ],
            [
                'name' => 'Wtf_pets',
                'description' => 'Un thème loufoque mettant en avant les animaux de compagnie.',
                'bg_color' => '#6C3483',
                'url_logo' => 'https://example.com/wtfpets_logo.png',
                'url_img' => 'https://example.com/wtfpets_background.jpg',
                'font' => 'Comic Sans MS, cursive',
                'user_id' => $userId // Utilise le même ID utilisateur existant
            ],
        ]);
    }
}
