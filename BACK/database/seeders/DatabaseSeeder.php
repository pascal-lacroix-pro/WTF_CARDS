<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            ThemesSeeder::class,
            TypeSeeder::class,
            CardsSeeder::class,
            // Ajoutez d'autres seeders ici si nécessaire.
        ]);
    }
}
