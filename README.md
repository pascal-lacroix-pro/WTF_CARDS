# WTF_CARDS

## Introduction

Bienvenue sur le projet **WTF_CARDS** ! Ce projet vise à développer une application de jeu de cartes interactif, où les joueurs peuvent créer et collectionner des cartes. Il peuvent également s'affronter dans des duels stratégiques. **WTF_CARDS** se distingue par son univers déjanté et décalé.

Le jeu est développé en utilisant une pile technologique moderne, combinant le framework web **Laravel** pour le backend et **VueJS** avec **ViteJS** pour une expérience utilisateur fluide et réactive sur le frontend. La connexion se fera grâce au package **Sanctum**. **Docker** va permettre d'encapsuler le développement pour une meilleure intégration locale. Ce choix technologique permet de tirer parti des meilleures fonctionnalités de ces écosystèmes, tout en assurant une performance optimale et une maintenabilité du code.

Notre objectif est de créer une plateforme ludique et compétitive, accessible à tous, où les joueurs peuvent mettre à l'épreuve leurs compétences stratégiques, tout en développant leur collection de cartes. Que vous soyez un joueur chevronné à la recherche de nouveaux défis ou un novice désireux de découvrir le monde fascinant des jeux de cartes, **WTF_CARDS** offre une expérience enrichissante et captivante.

Ce document README contient toutes les informations nécessaires pour commencer à travailler sur le projet, y compris les instructions d'installation, la configuration, la structure du projet, et les directives de contribution. Suivez les étapes décrites ci-dessous pour configurer votre environnement de développement et plonger dans le développement de **WTF_CARDS**.

## Installation

### Prérequis
- Docker
- Git

### Cloner le projet en local
Pour commencer, clonez le dépôt GitLab sur votre machine locale en via Gitkraken

### Configuration de l'environnement

Une fois le projet cloné, naviguez dans le dossier du projet et utilisez Docker pour construire et lancer les conteneurs nécessaires pour le backend Laravel et le frontend VueJS :

```bash
docker-compose up -d
```

### Structure du projet

Le projet est structuré en deux répertoires principaux : back pour le backend Laravel et front pour le frontend VueJS. Chaque répertoire contient le code source et les fichiers de configuration spécifiques à chaque partie de l'application.

```bash
WTF_CARDS/
│
├── back/ # Dossier pour le backend Laravel
│   ├── app/
│   ├── bootstrap/
│   ├── config/
│   ├── database/
│   ├── public/
│   ├── resources/
│   ├── routes/
│   ├── storage/
│   ├── tests/
│   ├── .env
│   ├── .env.example
│   ├── artisan
│   ├── composer.json
│   ├── docker-compose.yml # Docker compose pour le backend
│   └── Dockerfile # Dockerfile pour construire l'image Laravel
│
├── front/ # Dossier pour le frontend VueJS
│   ├── public/
│   ├── src/
│   │   ├── assets/
│   │   ├── components/
|   │   │   ├── BaseButton.vue
|   │   │   ├── CardItem.vue
|   │   │   ├── AddCardForm.vue
│   │   ├── views/
|   │   │   ├── Dashboard.vue
|   │   │   ├── CardList.vue
|   │   │   ├── AddCard.vue
|   │   │   ├── ThemeDescription.vue
|   │   │   ├── UserProfile.vue
|   │   │   ├── UserDecks.vue
│   │   ├── store/
|   │   │   ├── index.js
|   │   │   ├── cards.js
|   │   │   ├── themes.js
|   │   │   ├── user.js
|   │   │   ├── decks.js
│   │   ├── router/
|   │   │   ├── index.js
│   │   ├── App.vue
│   │   └── main.js
│   ├── .env
│   ├── .env.example
│   ├── index.html
│   ├── package.json
│   ├── vite.config.js
│   ├── docker-compose.yml # Docker compose pour le frontend
│   └── Dockerfile # Dockerfile pour construire l'image VueJS
│
├── .gitignore
├── docker-compose.yml # Docker compose principal pour orchestrer les services
└── README.md
```

### Contribution

#### Workflow

Nous utilisons GitLab pour la gestion de notre code source et notre collaboration. Voici comment nous travaillons :

1. S'attribuer une issue : Parcourez les issues disponibles dans le projet GitLab et attribuez-vous celle que vous souhaitez prendre en charge.
   
2. Créer une nouvelle branche : Basée sur main, créez une nouvelle branche pour votre travail. Utilisez un nom descriptif pour votre branche, par exemple feature/nouvelle-fonctionnalite.
   
3. Développement : Effectuez vos modifications dans votre branche. Assurez-vous de suivre les conventions de codage et d'écrire des tests si nécessaire.

4. Tester votre travail : Avant de soumettre votre code, testez-le localement pour vous assurer qu'il fonctionne comme prévu et qu'il ne casse pas d'autres parties de l'application.

5. Créer une merge request : Une fois votre travail terminé et testé, créez une merge request vers main. Assurez-vous d'inclure une description détaillée de vos changements.

6. Revue de code : Un ou plusieurs membres de l'équipe examineront votre merge request. Il se peut que vous receviez des commentaires ou des demandes de modifications.

7. Intégration : Après l'approbation de votre merge request, elle sera fusionnée dans main.

#### Sprints et User Stories
Nous organisons notre travail en sprints, chacun durant deux semaines. Chaque sprint est composé de plusieurs user stories, qui définissent les fonctionnalités à développer.

### Ressources

- Documentation Laravel
- Documentation VueJS
- Documentation ViteJS

Nous vous encourageons à lire ces documentations pour vous familiariser avec les technologies utilisées dans ce projet.

### Support

Si vous rencontrez des difficultés ou avez des questions, n'hésitez pas à ouvrir une issue dans le projet GitLab ou à contacter un membre de l'équipe.

Nous sommes impatients de voir vos contributions et de travailler ensemble à la réalisation de WTF_CARDS. Bon développement !

