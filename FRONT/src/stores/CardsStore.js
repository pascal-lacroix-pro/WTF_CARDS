import { defineStore } from 'pinia'
import axios from 'axios'

export const useCardsStore = defineStore('cards', {
  state: () => ({
    cards: [],
  }),
  actions: {
    async getCards() {
      try {
        const response = await axios.get('http://localhost:8000/api/cards');
        this.cards = response.data;
      } catch (error) {
        console.error('Il y a eu une erreur lors de la récupération des cartes :', error);
      }
    },
  },
});