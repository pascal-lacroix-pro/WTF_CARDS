import { createRouter, createWebHistory } from 'vue-router'
import AppLayout from '@/layouts/AppLayout.vue'
import AddCard from '@/views/AddCard.vue'
import Dashboard from '@/views/Dashboard.vue'
import UserProfile from '@/views/UserProfile.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: AppLayout,
      children: [
        {
          path: '',
          name: 'Dashboard',
          component: Dashboard,
        },
        {
          path: 'add-card',
          name: 'AddCard',
          component: AddCard,
        },
        {
          path: 'user-profile',
          name: 'UserProfile',
          component: UserProfile,
        },
      ],
    },
    
  ]
})

export default router
